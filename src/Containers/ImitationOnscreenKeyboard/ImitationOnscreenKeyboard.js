import React, {Component} from 'react';
import './ImitationOnscreenKeyboard.css';

import {connect} from "react-redux";
import numbers from '../../store/numbers';
import actionTypes from "../../store/actionTypes";
import Symbol from "../../Components/Symbol/Symbol";

const rightPin = '1111';

class ImitationOnscreenKeyboard extends Component {

  validationOfLength = (type) => {
    this.props.pin.length < 4
      ? this.props.numbers(type)
      : null;
  };

  confirmRightPin = () => {
    if (this.props.pin === rightPin) {
      this.props.enter();
    } else if (this.props.pin.length === 0) {
      alert('Enter the pin code');
    } else {
      alert('Wrong pin code! Try again');
    }
  };

  render() {
    return(
      <div className='Keyboard'>
        <h3 style={{textAlign: 'center'}}>Imitation onscreen keyboard</h3>
        <span className='Display-PinCode' style={{background: this.props.entered ? 'yellowgreen' : null}}>
          {this.props.entered
            ? 'Access Granted'
            : new Array(this.props.pin.length).fill('*').join('')}
            </span>
        {numbers.map(type => {
          return <Symbol clicked={() => this.validationOfLength(type)}
                         class='Symbol' key={type}
                         type={type}/>
        })}
        <Symbol class='Symbol'
                clicked={this.props.remove}
                type='<'/>
        <Symbol class='Symbol'
                clicked={() => this.confirmRightPin()}
                type='E'/>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    pin: state.pin,
    entered: state.entered,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    numbers: (number) => dispatch({type: actionTypes.CLICK, number}),
    remove: () => dispatch({type: actionTypes.REMOVE}),
    enter: () => dispatch({type: actionTypes.ENTER}),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ImitationOnscreenKeyboard);