import React, {Component} from 'react';
import './Calculator.css';

import {connect} from "react-redux";
import actionTypes from "../../store/actionTypes";
import numbers from '../../store/numbers';
import Symbol from "../../Components/Symbol/Symbol";

class Calculator extends Component {
  render() {
    return(
      <div className='Calculator'>
        <h3>Calculator</h3>
        <div className='Expression-Form'><span className='Display-Expression'>{this.props.result}</span></div>
        <div className='Numbers'>
          {numbers.map(number => {
            return <Symbol clicked={() => this.props.numbers(number)}
                           class='Symbol' key={number}
                           type={number}/>
          })}
          <Symbol type='=' class='Symbol' clicked={this.props.enter}/>
          <Symbol type='C' class='Symbol' clicked={this.props.clear}/>
        </div>
        <div className='Symbols'>
          <Symbol type='+' class='Symbol' clicked={this.props.addition}/>
          <Symbol type='-' class='Symbol' clicked={this.props.subtraction}/>
          <Symbol type='*' class='Symbol' clicked={this.props.multiplication}/>
          <Symbol type='/' class='Symbol' clicked={this.props.division}/>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    result: state.result,
    entered: state.entered,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    numbers: (number) => dispatch({type: actionTypes.CLICKED, number}),
    addition: () => dispatch({type: actionTypes.ADDITION}),
    subtraction: () => dispatch({type: actionTypes.SUBTRACTION}),
    multiplication: () => dispatch({type: actionTypes.MULTIPLICATION}),
    division: () => dispatch({type: actionTypes.DIVISION}),
    remove: () => dispatch({type: actionTypes.REMOVED}),
    enter: () => dispatch({type: actionTypes.ENTERED}),
    clear: () => dispatch({type: actionTypes.CLEAR}),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);