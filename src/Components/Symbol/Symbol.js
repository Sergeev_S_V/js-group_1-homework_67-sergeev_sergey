import React from 'react';
import './Symbol.css';

const Symbol = props => (
  <button className={props.class} onClick={props.clicked}>{props.type}</button>
);

export default Symbol;