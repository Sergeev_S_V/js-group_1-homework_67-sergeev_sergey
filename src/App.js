import React, { Component } from 'react';
import ImitationOnscreenKeyboard from "./Containers/ImitationOnscreenKeyboard/ImitationOnscreenKeyboard";
import Calculator from "./Containers/Calculator/Calculator";

class App extends Component {
  render() {
    return (
      <div className="App">
        <ImitationOnscreenKeyboard/>
        <Calculator/>
      </div>
    );
  }
}

export default App;
