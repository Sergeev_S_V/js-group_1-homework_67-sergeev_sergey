export default {
  // action for ImitationOnscreenKeyboard
  CLICK: 'CLICK',
  REMOVE: 'REMOVE',
  ENTER: 'ENTER',
  // action for Calculator
  CLICKED: 'CLICKED',
  REMOVED: 'REMOVED',
  ENTERED: 'ENTERED',
  ADDITION: 'ADDITION',
  SUBTRACTION: 'SUBTRACTION',
  MULTIPLICATION: 'MULTIPLICATION',
  DIVISION: 'DIVISION',
  CLEAR: 'CLEAR',
}