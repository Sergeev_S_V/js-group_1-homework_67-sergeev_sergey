import actionTypes from "./actionTypes";

const initialState = {
  // state for ImitationOnscreenKeyboard
  pin: '',
  entered: false,
  // state for Calculator
  result: '',
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    // action for ImitationOnscreenKeyboard
    case actionTypes.CLICK:
      return {...state, pin: state.pin + action.number};
    case actionTypes.REMOVE:
      return {...state,
        pin: state.pin.substring(0, state.pin.length - 1),
        entered: false};
    case actionTypes.ENTER:
      return {...state, entered: true};
    // action for Calculator
    case actionTypes.CLICKED:
      return {...state, result: state.result + action.number};
    case actionTypes.ADDITION:
      return {...state, result: state.result + '+'};
    case actionTypes.SUBTRACTION:
      return {...state, result: state.result + '-'};
    case actionTypes.MULTIPLICATION:
      return {...state, result: state.result + '*'};
    case actionTypes.DIVISION:
      return {...state, result: state.result + '/'};
    case actionTypes.ENTERED:
      return {...state, result: eval(state.result)};
    case actionTypes.CLEAR:
      return {...state, result: state.result = ''};
    default:
      return state;
  }
};

export default reducer;